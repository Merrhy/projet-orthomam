# Projet Orthomam

## Contexte

Le projet Orthomam est un projet donnée au sein du Master 1 Bioinformatique de l'université de Montpellier où le but est de calculer les taux
d'évolution des exons de différentes espèces d'intérêts à l'aide d'arbres phylogénétiques (Maximum Likelihood Trees) obtenus grâce à la base de 
données Orthomam (https://orthomam.mbb.cnrs.fr).

Pour se faire, l'utilisation du programme R (https://cran.r-project.org) et de son package APE https://cran.r-project.org/web/packages/ape/index.html) a été fortement recommandée.
